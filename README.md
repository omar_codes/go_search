# Go Search

## Requerimientos
* Go 1.12
* Node JS 12

## Instalación
Configura las variables de entorno ```GOPATH``` y ```GOROOT```, clona este repositorio en el directorio ```GOPATH/src/gitlab.com/omar_codes```

Copia el contenido del archivo ```.env.example``` en un nuevo archivo llamado ```.env```
```
npm install
npm run build
npm run doc

go get -u github.com/gofiber/fiber
go get -u github.com/gofiber/cors
go run ./main.go
```
## URLS
* ```/``` - Aplicación cliente del API
* ```/doc``` - Documentación del API