package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

// MazeResponse respueta desde el API
type MazeResponse struct {
	Score float32 `json:"score"`
	Show  struct {
		ID           int    `json:"id"`
		Name         string `json:"name"`
		Status       string `json:"status"`
		Premiered    string `json:"premiered"`
		OfficialSite string `json:"officialSite"`
		Summary      string `json:"summary"`
		Imagen       struct {
			Medium   string `json:"medium"`
			Original string `json:"original"`
		} `json:"image"`
	} `json:"show"`
}

// SearchTVShow Busca shows de TV
/**
* @api {get} /tvshows buscar shows de TV
* @apiName SearchTVShow
* @apiGroup Search
*
* @apiDescription busca shows de TV por nombre
* @apiParam {String} term término de busquesa ej:
* ```
* /tvshows?term=breaking
* ```
* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
[
	"score": 17.469301,
	"show": {
		"id": 169,
		"name": "Breaking Bad",
		"status": "Ended",
		"premiered": "2008-01-20",
		"officialSite": "http://www.amc.com/shows/breaking-bad",
		"summary": "<p><b>Breaking Bad</b> follows protagonist Walter White, a chemistry teacher who lives in New Mexico with his wife and teenage son who has cerebral palsy. White is diagnosed with Stage III cancer and given a prognosis of two years left to live. With a new sense of fearlessness based on his medical prognosis, and a desire to secure his family's financial security, White chooses to enter a dangerous world of drugs and crime and ascends to power in this world. The series explores how a fatal diagnosis such as White's releases a typical man from the daily concerns and constraints of normal society and follows his transformation from mild family man to a kingpin of the drug trade.</p>",
		"image": {
			"medium": "http://static.tvmaze.com/uploads/images/medium_portrait/0/2400.jpg",
			"original": "http://static.tvmaze.com/uploads/images/original_untouched/0/2400.jpg"
		}
	},
	...
]
*/
func SearchTVShow(term string) ([]MazeResponse, error) {
	searchURL := "http://api.tvmaze.com/search/shows?q=" + url.PathEscape(term)
	res, err := http.Get(searchURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var response []MazeResponse
	json.Unmarshal(body, &response)

	return response, nil
}
