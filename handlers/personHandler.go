package handlers

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"net/url"
)

// PersonResponse respuesta del servicio SOAP
type PersonResponse struct {
	XMLName xml.Name
	Body    Body
}

// Body cuerpo de la respuesta
type Body struct {
	XMLName     xml.Name
	GetResponse ListByNameResponse `xml:"GetListByNameResponse"`
}

// ListByNameResponse lista de respuestas
type ListByNameResponse struct {
	GetResult ListByNameResult `xml:"GetListByNameResult"`
}

// ListByNameResult Lista de resultados
type ListByNameResult struct {
	ListPerson []Person `xml:"PersonIdentification"`
}

// Person datos de la persona
type Person struct {
	ID   int    `xml:"id"`
	Name string `xml:"Name"`
	SSN  string `xml:"SSN"`
	DOB  string `xml:"DOB"`
}

// SearchPerson buscar persona por nombre
/**
* @api {get} /persons buscar personas por nombre
* @apiName SearchPerson
* @apiGroup Search
*
* @apiDescription busca personas por nomnre
* @apiParam {String} term término de busquesa ej:
* ```
* /persons?term=newton
* ```
* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
[
	{
		"ID": 0,
		"Name": "Newton,Dave R.",
		"SSN": "384-10-6538",
		"DOB": "2000-03-20"
	}
]
*/
func SearchPerson(term string) ([]Person, error) {
	searchURL := "http://www.crcind.com/csp/samples/SOAP.Demo.cls?soap_method=GetListByName&name=" + url.PathEscape(term)
	res, err := http.Get(searchURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var response PersonResponse
	xml.Unmarshal(body, &response)
	return response.Body.GetResponse.GetResult.ListPerson, nil
}
