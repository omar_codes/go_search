package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

// ItunesTracks respuesta para canciones
type ItunesTracks struct {
	Count   int     `json:"resultCount"`
	Results []Track `json:"results"`
}

// Track informacion de cancion
type Track struct {
	TrackID        int    `json:"trackId"`
	ArtistName     string `json:"artistName"`
	CollectionName string `json:"collectionName"`
	TrackName      string `json:"trackName"`
	ArtworkURL     string `json:"artworkUrl100"`
	TrackView      string `json:"trackViewUrl"`
}

// ItunesMovies respuesta para peliculas
type ItunesMovies struct {
	Count   int     `json:"resultCount"`
	Results []Movie `json:"results"`
}

// Movie informacion de pelicula
type Movie struct {
	TrackID        int    `json:"trackId"`
	ArtistName     string `json:"artistName"`
	CollectionName string `json:"collectionName"`
	TrackName      string `json:"trackName"`
	ArtworkURL     string `json:"artworkUrl100"`
	Description    string `json:"shortDescription"`
	PreviewURL     string `json:"previewUrl"`
	TrackView      string `json:"trackViewUrl"`
}

// SearchItunesTracks busca canciones en itunes
/**
* @api {get} /tracks buscar canciones en itunes
* @apiName SearchItunesTracks
* @apiGroup Search
*
* @apiDescription busca canciones en itunes por nombre
* @apiParam {String} term término de busquesa ej:
* ```
* /tracks?term=linkin
* ```
* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
[
	{
		"trackId": 185731686,
		"artistName": "JAY-Z & LINKIN PARK",
		"collectionName": "Collision Course - EP (Deluxe Version)",
		"trackName": "Numb / Encore",
		"artworkUrl100": "https://is4-ssl.mzstatic.com/image/thumb/Music71/v4/1f/5f/69/1f5f6936-eec0-ef3f-fe9f-830c9b1a3c9a/source/100x100bb.jpg",
		"trackViewUrl": "https://music.apple.com/us/album/numb-encore/185731554?i=185731686&uo=4"
	},
	...
]
*/
func SearchItunesTracks(term string) ([]Track, error) {
	searchURL := "https://itunes.apple.com/search?media=music&term=" + url.PathEscape(term)
	res, err := http.Get(searchURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var response ItunesTracks
	json.Unmarshal(body, &response)
	return response.Results, nil
}

// SearchItunesMovies busca peliculas en itunes
/**
* @api {get} /movies buscar películas en itunes
* @apiName SearchItunesMovies
* @apiGroup Search
*
* @apiDescription busca películas en itunes por nombre
* @apiParam {String} term término de busquesa ej:
* ```
* /movies?term=avengers
* ```
* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
[
	{
		"trackId": 1370224078,
		"artistName": "Anthony Russo & Joe Russo",
		"collectionName": "Avengers - 4 Film Collection",
		"trackName": "Avengers: Infinity War",
		"artworkUrl100": "https://is5-ssl.mzstatic.com/image/thumb/Video124/v4/fc/f4/1f/fcf41fb8-da97-0d32-7f57-f630d25cd5ab/source/100x100bb.jpg",
		"shortDescription": "An unprecedented cinematic journey ten years in the making and spanning the entire Marvel Cinematic",
		"previewUrl": "https://video-ssl.itunes.apple.com/itunes-assets/Video115/v4/ab/3b/9a/ab3b9ac8-e641-50f0-d0c5-70ac3f90ea4f/mzvf_5172850007233101562.640x354.h264lc.U.p.m4v",
		"trackViewUrl": "https://itunes.apple.com/us/movie/avengers-infinity-war/id1370224078?uo=4"
	},
	...
]
*/
func SearchItunesMovies(term string) ([]Movie, error) {
	searchURL := "https://itunes.apple.com/search?media=movie&term=" + url.PathEscape(term)
	res, err := http.Get(searchURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var response ItunesMovies
	json.Unmarshal(body, &response)
	return response.Results, nil
}
