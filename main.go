package main

import (
	"github.com/gofiber/cors"
	"github.com/gofiber/fiber"
	"gitlab.com/omar_codes/go_search/handlers"
)

func main() {
	app := fiber.New()

	app.Use(cors.New())
	app.Get("/tvshows", getTVShows)
	app.Get("/tracks", getTracks)
	app.Get("/movies", getMovies)
	app.Get("/persons", getPersons)
	app.Static("/", "./dist")
	app.Static("/", "./doc")
	app.Static("/doc", "./doc")

	app.Listen(3000)
}

func getTVShows(c *fiber.Ctx) {
	term := c.Query("term", "")
	if term == "" {
		c.Status(400).Send("bad request")
		return
	}
	var response []handlers.MazeResponse
	response, err := handlers.SearchTVShow(term)
	if err != nil {
		c.Status(500).Send(err)
		return
	}
	err = c.JSON(response)
	if err != nil {
		c.Status(500).Send(err)
		return
	}
}

func getTracks(c *fiber.Ctx) {
	term := c.Query("term", "")
	if term == "" {
		c.Status(400).Send("bad request")
		return
	}
	var response []handlers.Track
	response, err := handlers.SearchItunesTracks(term)
	if err != nil {
		c.Status(500).Send(err)
		return
	}
	err = c.JSON(response)
	if err != nil {
		c.Status(500).Send(err)
		return
	}
}

func getMovies(c *fiber.Ctx) {
	term := c.Query("term", "")
	if term == "" {
		c.Status(500).Send("bad request")
		return
	}
	var response []handlers.Movie
	response, err := handlers.SearchItunesMovies(term)
	if err != nil {
		c.Status(500).Send(err)
		return
	}
	err = c.JSON(response)
	if err != nil {
		c.Status(500).Send(err)
		return
	}
}

func getPersons(c *fiber.Ctx) {
	term := c.Query("term", "")
	if term == "" {
		c.Status(500).Send("bad request")
		return
	}
	var response []handlers.Person
	response, err := handlers.SearchPerson(term)
	if len(response) > 0 {
		err = c.JSON(response)
		if err != nil {
			c.Status(500).Send(err)
			return
		}
	} else {
		res := []string{}
		c.JSON(res)
	}
}
