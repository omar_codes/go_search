import axios from 'axios'

const baseURL = process.env.VUE_APP_API_URL || 'http://localhost:3000'

const api = axios.create({ baseURL })

const services = {
  $api: {
    get () {
      return api
    }
  }
}

export default (Vue) => {
  Object.defineProperties(Vue.prototype, services)
}
