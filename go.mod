module gitlab.com/omar_codes/go_search

go 1.12

require (
	github.com/gofiber/cors v0.2.2
	github.com/gofiber/fiber v1.13.3
	github.com/klauspost/compress v1.10.10 // indirect
	github.com/yuin/goldmark v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200808120158-1030fc2bf1d9 // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/tools v0.0.0-20200809012840-6f4f008689da // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
